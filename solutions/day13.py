from datetime import datetime

class Cart():
    def __init__(self, x, y, d):
        self.x = x
        self.y = y
        self.d = d #<,>,^,v
        self.lt = 2 #0, 1, 2 (left, straight, right)

    def __str__(self):
        return "Dir: {}, ({},{})".format(self.d, self.x, self.y)

    def __repr__(self):
        return self.__str__()

    def move(self, grid):
        oldt = grid[self.x][self.y]
        # print("Moving... Dir:",self.d,oldt)
        if self.d == "v":
            self.y +=1
        elif self.d == "^":
            self.y -=1
        elif self.d == "<":
            self.x -=1
        elif self.d == ">":
            self.x +=1
        t = grid[self.x][self.y]
        if t not in r"-|\/+":
            raise RuntimeError("Unknown grid type: '{}' from '{}'. Dir: {}".format(t, oldt, self.d))
        dt = self.d + t #Direction and type
        if dt in [">/", "^\\", "</", "v\\"]: #Left Turns
            # print("Changing dir left:", self.d, oldt, t)
            self.left()
            # print("New dir:", self.d)
        elif dt in ["<\\", ">\\", "^/", "v/"]: #Right turns
            # print("Changing dir right:", self.d, oldt, t)
            self.right()
            # print("New dir:", self.d)
        if grid[self.x][self.y] == "+":
            # print("At cross")
            if self.lt == 2:
                self.left()
            elif self.lt == 1:
                self.right()
            self.lt = (self.lt + 1) % 3
    def left(self):
        if self.d == "^":
            self.d = "<"
        elif self.d == "<":
            self.d = "v"
        elif self.d == "v":
            self.d = ">"
        elif self.d == ">":
            self.d = "^"
    def right(self):
        if self.d == "^":
            self.d = ">"
        elif self.d == ">":
            self.d = "v"
        elif self.d == "v":
            self.d = "<"
        elif self.d == "<":
            self.d = "^"


def main():
    with open("solutions/day13input.txt", "r") as f:
        lines = [l.rstrip("\n") for l in f]
        start = datetime.utcnow()
        solve(lines)
        duration = datetime.utcnow() - start
        print(duration)

def solve(data):
    dim = len(data)
    grid = [[""]*dim for _ in range(dim)]
    carts = []
    for y,l in enumerate(data):
        for x,c in enumerate(l):
            if c in "v^":
                carts.append(Cart(x,y,c))
                c = "|"
            if c in "<>":
                carts.append(Cart(x,y,c))
                c = "-"
            grid[x][y] = c
    while len(carts) > 1:
        crashes = set() #Reset crashes every tick (clear the track)
        cartlocs = set()
        for c in carts:
            c.move(grid)
            if (c.x,c.y) in cartlocs:
                crashes.add((c.x,c.y))
                print("Crash:",c.x,c.y)
            cartlocs.add((c.x,c.y))
        carts = [c for c in carts if (c.x,c.y) not in crashes] #Remove crashed carts from processing
        carts.sort(key=lambda c:c.y)
    print(carts)
    

if __name__ == "__main__":
    main()
