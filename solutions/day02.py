from collections import Counter

def main():
    with open("solutions/day2input.txt", "r") as f:
        part1(f)
        f.seek(0)
        part2(f)

def part1(f):
    twos = 0
    threes = 0
    for line in f:
        counts = set(Counter(line).values())
        if 2 in counts:
            twos +=1
        if 3 in counts:
            threes +=1
    print(twos * threes)

def part2(f):
    lines = f.readlines()
    matches = set()
    for i in range(len(lines)):
        for j in range(i+1, len(lines)):
            diffs = 0
            for c1,c2 in zip(lines[i],lines[j]):
                if c1 != c2:
                    diffs +=1
            if diffs == 1:
                matches.add(lines[i].strip())
                matches.add(lines[j].strip())
    print(matches)

            
if __name__ == "__main__":
    main()
