from collections import Counter, defaultdict
import re

def main():
    with open("solutions/day04input.txt", "r") as f:
        data = [l.strip() for l in f]
        data.sort()
        solve(data)
    
def solve(data):
    guards = defaultdict(Counter)
    g_id = 0
    sleep = 0
    for i in data:
        minute = re.search(r'\d+:(\d+)',i).group(1)
        if "Guard" in i:
            g_id = re.search(r'Guard #(\d+)', i).group(1)
            continue
        if "wakes" in i:
            guards[g_id].update([x for x in range(sleep, int(minute))])
        else:
            sleep = int(minute)
    
    #Part 1
    _, g_id = max((sum(c.values()), g) for g, c in guards.items())
    print("Guard ID:",g_id, "Minute:", guards[g_id].most_common(1)[0][0])

    #Part 2
    _, g_id = max((c.most_common()[0][1], g) for g, c in guards.items())
    print("Guard ID:",g_id, "Minute:",guards[g_id].most_common(1)[0][0])


if __name__ == "__main__":
    main()
