from datetime import datetime
from collections import defaultdict
import re
initstate = "#.#.#..##.#....#.#.##..##.##..#..#...##....###..#......###.#..#.....#.###.#...#####.####...#####.#.#"

def main():
    with open("solutions/day12input.txt", "r") as f:
        lines = [l.strip() for l in f]
        start = datetime.utcnow()
        solve(lines)
        duration = datetime.utcnow() - start
        print(duration)

def solve(data):
    pots = defaultdict(lambda:".")
    _,p = build_pattern_sets(data)
    for n,v in enumerate(initstate): #Build initial state
        pots[n] = v
    csum = 0
    for g in range(1,21):
        yes = []
        mi,ma = min(pots.keys()),max(pots.keys())
        for y in range(mi, ma+4):
            patt = "".join([pots[y+i] for i in [-2,-1,0,1,2]])
            if patt in p:
                yes.append(y)
        for k in pots:
            pots[k] = "#" if k in yes else "."
        psum = csum
        csum = sum([k for k,v in pots.items() if v=="#"])
        print("Gen:{} Sum:{} Diff:{}".format(g, csum, csum-psum))

def build_pattern_sets(lines):
    np = set() #No Plants
    p = set() #Plants
    for l in lines: #Build the patterns for growth/no growth
        if "=>" not in l:
            continue
        pat, res = l.split("=>")
        if "." in res:
            np.add(pat.strip())
        else:
            p.add(pat.strip())
    return (np,p)

if __name__ == "__main__":
    main()
