from datetime import datetime
import re

def main():
    regex = r"position=< *(-?\d+), *(-?\d+)> velocity=< *(-?\d+), *(-?\d+)>"
    with open("solutions/day10input.txt", "r") as f:
        lines = [x.strip() for x in f]
        points = []
        for l in lines:
            x,y,vx,vy = map(int, re.match(regex, l).groups())
            points.append([x,y,vx,vy])
        start = datetime.utcnow()
        solve(points)
        duration = datetime.utcnow() - start
        print(duration)

def solve(points):
    window = 100
    for t in range(11000):
        minx = min([x for x,y,_,_ in points])
        maxx = max([x for x,y,_,_ in points])
        miny = min([y for x,y,_,_ in points])
        maxy = max([y for x,y,_,_ in points])
        if minx + window >= maxx and miny+window >= maxy:
            print(t, minx, maxx, miny, maxy)
            for y in range(miny, maxy+1):
                for x in range(minx, maxx+1):
                    if (x,y) in [(x,y) for x,y,_,_ in points]:
                        print("#",end="")
                    else:
                        print(".",end="")
                print("")

        for p in points:
            p[0] += p[2]
            p[1] += p[3]
    


if __name__ == "__main__":
    main()
