from datetime import datetime

nums = []
cmeta = [] #Cumulative metadata

def main():
    global nums
    with open("solutions/day08input.txt", "r") as f:
        lines = [x for x in f]
        nums = [int(x) for x in lines[0].split()]
        start = datetime.utcnow()
        solve()
        duration = datetime.utcnow() - start
        print(duration)
    
def solve():
    _, val = read(0)
    print(sum(cmeta))
    print(val)

def read(idx):
    c = nums[idx]
    m = nums[idx+1]
    c_vals = [0 for x in range(c)]
    m_vals = []
    value = 0

    idx +=2
    for x in range(c): #Recurse the children
        idx, val = read(idx)
        c_vals[x] = val

    for y in range(m):
        metaval = nums[idx+y]
        m_vals.append(metaval)
        cmeta.append(metaval)

    if c == 0: #Has no children
        value = sum(m_vals)
    else:
        for x in m_vals:
            x-=1
            if x < len(c_vals):
                value += c_vals[x]
    
    return idx + m, value

if __name__ == "__main__":
    main()
