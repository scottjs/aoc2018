from collections import defaultdict
from datetime import datetime
def main():
    with open("solutions/day06input.txt", "r") as f:
        coords = []
        for line in f:
            x,y = line.strip().split(",")
            coords.append((int(x), int(y)))
        start = datetime.utcnow()
        solve(coords)
        duration = datetime.utcnow() - start
        print(duration)
    
def solve(data: list):
    min_x = min([i[0] for i in data])
    min_y = min([i[1] for i in data])
    max_x = max([i[0] for i in data])
    max_y = max([i[1] for i in data])
    areas = defaultdict(list)
    exclude = set()
    safe = [] #Part 2
    
    for x in range(max_x+1):
        for y in range(max_y+1):
            dist = 5000
            dists = []
            closest = None
            for p in data:
                nd = distance(p, (x,y))
                dists.append(nd)
                if nd < dist:
                    closest = p
                    dist = nd
            if sum(dists) < 10000: #Part 2
                safe.append((x,y)) #Part 2
            if dists.count(min(dists)) == 1:
                areas[closest].append((x,y))
                if is_edge(x, y, min_x, min_y, max_x, max_y):
                    exclude.add(closest)

    la = [len(v) for k,v in areas.items() if k not in exclude]
    print("Part 1, Largest Area:", max(la))
    print("Part 2, Safe Region:", len(safe))
    
def distance(p1: tuple, p2: tuple) -> int:
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])

def is_edge(x, y, minx, miny, maxx, maxy) -> bool:
    if x <= minx or x >= maxx:
        return True
    if y <= miny or y >= maxy:
        return True
    return False

if __name__ == "__main__":
    main()
