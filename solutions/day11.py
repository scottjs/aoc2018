from datetime import datetime
import re

gridsn = 8868

def main():
    start = datetime.utcnow()
    solve2()
    duration = datetime.utcnow() - start
    print(duration)

def solve():
    mp = 0
    for x in range(1,298):
        for y in range(1,298):
            p = sum([pc(x2, y) for x2 in range(x,x+3)])
            p += sum([pc(x2, y+1) for x2 in range(x,x+3)])
            p += sum([pc(x2, y+2) for x2 in range(x,x+3)])
            if p > mp:
                mp = p
                c = (x,y)
    print(mp, c)

def solve2():
    grid = [[pc(x,y) for y in range(1,301)] for x in range(1,301)]
    mp = 0
    mw = 0
    for w in range(3, 300): #Change second range arg to 4 to get part1
        print("Starting",w)
        for x in range(299-w):
            for y in range(299-w):
                p = sum([grid[x+x2][y+y2] for x2 in range(w) for y2 in range(w)])
                if p > mp:
                    mp = p
                    c = x+1,y+1
                    mw = w
                    print(mp,c,mw)
    print("Power:",mp, "Coord:",c, "Width:",mw)

def pc(x,y):
    rack = x+10
    p = (rack * y + gridsn) * rack
    p = (p // 100) % 10
    p -= 5
    return p

if __name__ == "__main__":
    main()
