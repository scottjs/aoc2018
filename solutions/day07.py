from collections import defaultdict
from datetime import datetime
from string import ascii_uppercase
import re

def main():
    with open("solutions/day07input.txt", "r") as f:
        lines = [line.strip() for line in f]
        start = datetime.utcnow()
        part2(lines)
        duration = datetime.utcnow() - start
        print(duration)
    
def solve(data):
    ds = {}
    done = []
    avail = []
    for line in data:
        p, c = re.match(r'Step (\w) .+ step (\w)', line).groups()
        ds.setdefault(p, set())
        ds.setdefault(c, set())
        ds[c].add(p)
    while len(ds) or len(avail):
        for c in ds:
            if not len(ds[c]) and c not in avail:
                avail.append(c)
        avail.sort(reverse=True)
        d1 = avail.pop()
        del ds[d1]
        done.append(d1)
        for c,v in ds.items():
            if d1 in v:
                ds[c].remove(d1)
    print("".join(done))

def part2(data):
    ds = {}
    times = {k:v+60 for v,k in enumerate(ascii_uppercase, 1)}
    avail = []
    time = 0
    workers = []
    for line in data:
        p, c = re.match(r'Step (\w) .+ step (\w)', line).groups()
        ds.setdefault(p, set())
        ds.setdefault(c, set())
        ds[c].add(p)
    
    while len(ds) or len(avail):
        for c in ds:
            if not len(ds[c]) and c not in avail:
                avail.append(c)
        avail.sort(reverse=True)
        while len(avail) and len(workers) < 5:
            j = avail.pop()
            workers.append((time+times[j], j))
            del ds[j]

        if time < min(workers)[0]:
            time = min(workers)[0]

        for t,v1 in workers:
            if t <= time:
                for c,v2 in ds.items():
                    if v1 in v2:
                        ds[c].remove(v1)
                workers.remove((t,v1))
    print(time)
        

if __name__ == "__main__":
    main()
