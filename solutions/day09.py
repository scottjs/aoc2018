from datetime import datetime

class MRing():
    def __init__(self, val):
        self.value = val
        self.next = self
        self.prev = self

    def add(self, value):
        cur = None
        score = 0
        if value % 23 == 0:
            rem = self
            for _ in range(7):
                rem = rem.prev
            cur = rem.next
            score = rem.value + value
            rem.prev.next = rem.next
            rem.next.prev = rem.prev
        else:
            one_c = self.next
            two_c = self.next.next
            cur = MRing(value)
            cur.prev = one_c
            cur.next = two_c
            one_c.next = cur
            two_c.prev = cur
        return cur, score #current position/marble


def main():
    num_players = 486
    max_marble = 70833 * 100
    scores = [0] * num_players
    ring = MRing(0)

    start = datetime.utcnow()
    player = 1
    for m in range(1, max_marble+1):
        ring, score = ring.add(m)
        scores[player] += score
        player += 1
        player = player % num_players
    print(max(scores))
    duration = datetime.utcnow() - start
    print(duration)

if __name__ == "__main__":
    main()
