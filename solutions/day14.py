from datetime import datetime

ipt = 84601
ipt2 = "084601"
# ipt2 = "147061"

def main():
    start = datetime.utcnow()
    solve()
    duration = datetime.utcnow() - start
    print(duration)

def solve():
    c1,c2 = 0,1
    r = [3,7]
    while True:
        n1,n2 = divmod(r[c1]+r[c2],10)
        if n1 > 0:
            r.append(n1)
        r.append(n2)
        c1 = (r[c1]+1+c1) % len(r)
        c2 = (r[c2]+1+c2) % len(r)
        # if len(r) > ipt+10:
        #     break
        rstr = "".join(str(x) for x in r[-7:])
        if rstr.find(ipt2) > -1:
            break
    print(r[-7:])
    print("Length: ",len(r))

if __name__ == "__main__":
    main()
