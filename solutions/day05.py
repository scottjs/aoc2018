
def main():
    with open("solutions/day05input.txt", "r") as f:
        data = f.readline().strip()
        print(solve(data))
        part2(data)
    
def solve(data):
    stack = []
    for c in data:
        if stack:
            prev = stack[-1]
            if prev != c and c.lower() == prev.lower():
                stack.pop()
                continue
        stack.append(c)
    return(len(stack))

def part2(data):
    lengths = []
    for c in set(data):
        newdata = data.replace(c,"").replace(c.upper(),"")
        lengths.append(solve(newdata))
    print("Shortest", min(lengths))
    

if __name__ == "__main__":
    main()
