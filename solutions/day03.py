from collections import defaultdict

def main():
    with open("solutions/day03input.txt", "r") as f:
        solve(f)

def parse(line):
    ids, _, offset, d = line.split()
    left, top = offset[:-1].split(",")
    width, height = d.split("x")
    return ids, int(left), int(top), int(width), int(height)

def solve(lines):
    data = [parse(line) for line in lines]
    overlaps = defaultdict(int)
    for _, l, t, w, h in data:
        for i in range(w):
            for j in range(h):
                overlaps[(i + l, j + t)] += 1

    total = 0
    for v in overlaps.values():
        if v > 1:
            total += 1

    # Part 1
    print(total)

    for ids, l, t, w, h in data:
        isValid = True
        for i in range(w):
            for j in range(h):
                if overlaps[(i + l, j + t)] != 1:
                    isValid = False
                    break
            if not isValid:
                break
        if isValid:
            # Part 2
            print(ids[1:])

if __name__ == "__main__":
    main()
